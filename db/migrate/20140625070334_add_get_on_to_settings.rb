class AddGetOnToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :get_on_foot, :text
    add_column :settings, :get_on_transport, :text
  end
end
