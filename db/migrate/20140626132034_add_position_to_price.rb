class AddPositionToPrice < ActiveRecord::Migration
  def change
    add_column :prices, :position, :integer

    Price.all.each do |price|
      position = price.id
      price.update_column(:position, position)
    end
  end
end
