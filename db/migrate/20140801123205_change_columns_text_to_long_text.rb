class ChangeColumnsTextToLongText < ActiveRecord::Migration
  def change
    change_column :therapeutic_services, :content, :text, :limit => 4294967295
    change_column :cosmetic_services, :content, :text, :limit => 4294967295
  end
end
