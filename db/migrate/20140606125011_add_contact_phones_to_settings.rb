class AddContactPhonesToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :contact_phones, :text
  end
end
