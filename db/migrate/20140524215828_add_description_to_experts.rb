class AddDescriptionToExperts < ActiveRecord::Migration
  def change
    add_column :experts, :description, :text
    remove_column :experts, :experience
    remove_column :experts, :address
    remove_column :experts, :education
    remove_column :experts, :speciality
    remove_column :experts, :awards
  end
end
