class CreateMetaTags < ActiveRecord::Migration
  def change
    create_table :meta_tags do |t|
      t.string :name
      t.text :value

      t.timestamps
    end
  end
end
