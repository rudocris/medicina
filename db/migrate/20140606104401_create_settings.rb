class CreateSettings < ActiveRecord::Migration

  def change
    create_table :settings do |t|
      t.integer :singleton_guard
      t.text :meta_title
      t.text :meta_keywords
      t.text :meta_description
      t.text :address
      t.text :copywrite
      t.text :email
      t.text :schedule
      t.text :phone
      t.timestamps
    end
    add_index(:settings, :singleton_guard, :unique => true)
    Settings.instance
  end

end