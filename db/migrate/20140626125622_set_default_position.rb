class SetDefaultPosition < ActiveRecord::Migration
  def change
    ServicePrice.all.each do |price|
      position = price.id
      price.update_column(:position, position)
    end
  end
end
