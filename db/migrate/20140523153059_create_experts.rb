class CreateExperts < ActiveRecord::Migration

  def change
    create_table :experts do |t|
      t.text :category
      t.text :experience
      t.text :address
      t.text :education
      t.text :speciality
      t.text :awards
      t.timestamps
    end
  end

end