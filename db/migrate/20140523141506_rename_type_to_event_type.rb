class RenameTypeToEventType < ActiveRecord::Migration
  def change
    rename_column :events, :type, :marker
  end
end
