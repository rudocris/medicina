class CreateServicePrices < ActiveRecord::Migration

  def change
    create_table :service_prices do |t|
      t.text :title
      t.integer :price_id
      t.string :time
      t.string :cost
      t.timestamps
    end
  end

end