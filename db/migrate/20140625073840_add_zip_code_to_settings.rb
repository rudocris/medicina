class AddZipCodeToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :zip_code, :string
  end
end
