class CreateTherapeuticServices < ActiveRecord::Migration

  def change
    create_table :therapeutic_services do |t|
      t.text :name
      t.text :content
      t.timestamps
    end
  end

end