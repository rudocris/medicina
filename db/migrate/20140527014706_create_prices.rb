class CreatePrices < ActiveRecord::Migration

  def change
    create_table :prices do |t|
      t.string :title
      t.timestamps
    end
  end

end