class AddDefaultGenderToExpert < ActiveRecord::Migration
  def change
    change_column :experts, :gender, :string, default: "m"
  end
end
