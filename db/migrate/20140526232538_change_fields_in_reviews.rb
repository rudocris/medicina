class ChangeFieldsInReviews < ActiveRecord::Migration
  def change
    change_column :reviews, :name, :string
    change_column :reviews, :email, :string
  end
end
