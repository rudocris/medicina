class AddPositionToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :position, :integer
    add_column :experts, :position, :integer
    add_column :videos, :position, :integer    
  end
end
