class AddPositionToServicePrices < ActiveRecord::Migration
  def change
    add_column :service_prices, :position, :integer
  end
end
