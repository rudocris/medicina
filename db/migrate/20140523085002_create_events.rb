class CreateEvents < ActiveRecord::Migration

  def change
    create_table :events do |t|
      t.datetime :date, null: false
      t.string :title, null: false
      t.text :content, null: false
      t.string :type
      t.timestamps
    end
  end

end