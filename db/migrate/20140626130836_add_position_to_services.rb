class AddPositionToServices < ActiveRecord::Migration
  def change
    add_column :cosmetic_services, :position, :integer
    add_column :therapeutic_services, :position, :integer

    CosmeticService.all.each do |service|
      position = service.id
      service.update_column(:position, position)
    end

    TherapeuticService.all.each do |service|
      position = service.id
      service.update_column(:position, position)
    end

  end
end
