class ChangeMetaTags < ActiveRecord::Migration
  def change
    remove_column :meta_tags, :name
    remove_column :meta_tags, :value
    add_column :meta_tags, :meta_title, :text
    add_column :meta_tags, :meta_keywords, :text
    add_column :meta_tags, :meta_description, :text
  end
end
