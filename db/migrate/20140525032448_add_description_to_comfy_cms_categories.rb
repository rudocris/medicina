class AddDescriptionToComfyCmsCategories < ActiveRecord::Migration
  def change
    add_column :comfy_cms_categories, :description, :text
  end
end
