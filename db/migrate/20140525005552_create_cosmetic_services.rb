class CreateCosmeticServices < ActiveRecord::Migration

  def change
    create_table :cosmetic_services do |t|
      t.text :name
      t.text :content
      t.timestamps
    end
  end

end