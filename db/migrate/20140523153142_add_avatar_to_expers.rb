class AddAvatarToExpers < ActiveRecord::Migration
  def change
    add_attachment :experts, :avatar
  end
end
