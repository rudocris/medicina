class FillGenders < ActiveRecord::Migration
  def change
    Expert.all.each do |expert|
      expert.update_attribute :gender, "m"
    end
  end
end
