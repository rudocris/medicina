class CreateReviews < ActiveRecord::Migration

  def change
    create_table :reviews do |t|
      t.text :name
      t.text :email
      t.text :content
      t.string :kind
      t.timestamps
    end
  end

end