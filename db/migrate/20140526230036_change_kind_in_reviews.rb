class ChangeKindInReviews < ActiveRecord::Migration
  def change
    change_column :reviews, :kind, :string, default: "written"
  end
end
