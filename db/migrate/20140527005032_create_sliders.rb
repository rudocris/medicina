class CreateSliders < ActiveRecord::Migration

  def change
    create_table :sliders do |t|
      t.string :title
      t.text :content
      t.text :url
      t.timestamps
    end
  end

end