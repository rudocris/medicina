class CreateVideos < ActiveRecord::Migration

  def change
    create_table :videos do |t|
      t.string :title
      t.text :url
      t.timestamps
    end
  end

end