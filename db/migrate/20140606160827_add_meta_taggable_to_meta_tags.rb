class AddMetaTaggableToMetaTags < ActiveRecord::Migration
  def change
    add_reference :meta_tags, :meta_taggable, index: true, polymorphic: true
  end
end
