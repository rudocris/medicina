class AddNotificationEmailToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :notification_emails, :text
  end
end
