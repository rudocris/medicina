class AddPositionToCategories < ActiveRecord::Migration
  def up
    add_column :comfy_cms_categories, :position, :integer, default: 0

    Comfy::Cms::Category.reset_column_information

    Comfy::Cms::Category.all.each_with_index { |category, i| category.update_attribute :position, i }
  end
end
