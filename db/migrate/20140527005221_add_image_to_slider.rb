class AddImageToSlider < ActiveRecord::Migration
  def change
    add_attachment :sliders, :image
  end
end
