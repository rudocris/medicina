class AddWordOfWisdomToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :word_of_wisdom, :text
  end
end
