#= require ./vendor/jquery.jcarousel-core.min
#= require ./vendor/jquery.jcarousel-control.min
#= require ./vendor/masonry.min
#= require_self

$(document).ready ->
  $('.colorbox').colorbox(
    photo: true
  )

  $('.b-services-gallery .b-services-gallery__list').jcarousel
    scroll: 1

  $('.b-services-gallery__prev').jcarouselControl
    target: '-=1'

  $('.b-services-gallery__next').jcarouselControl
    target: '+=1'


  $('.b-events-grid').masonry
    columnWidth: 480,
    itemSelector: 'li'

  $(document).on 'click', '.b-box__toggle, .b-box__title a', (e) ->
    e.preventDefault()
    box = $(this).parents('.b-box, .b-box-taggable')
    if box.hasClass('is-active')
      box.removeClass('is-active')
    else
      box.addClass('is-active')