module ExpertsHelper
  def expert_image(expert, size)  
    return image_tag(expert.avatar.url(size)) if expert.avatar.exists?
    
    if expert.gender == "m"
      size == :lg ? image_tag("big_man.png") : image_tag("man.png")
    else
      size == :lg ? image_tag("big_woman.png") : image_tag("woman.png")
    end
  end
end
