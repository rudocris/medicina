module ApplicationHelper
  def meta(name)
    if @expert
      @expert.meta_tag.send(name)
    else
      Settings.instance.send(name)
    end
  end

  def html_safe_snippet(name)
    raw(cms_snippet_content(name))
  end

  def highlight_active(path)
    {class: "is-active"} if request.path[path]
  end

  def highlight_inline_active(path)
    {class: "is-active"} if request.path == path
  end
end
