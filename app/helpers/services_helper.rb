module ServicesHelper
  def edit_page(page)
    edit_comfy_admin_cms_site_page(1, Comfy::Cms::Page.find_by_full_path(page.full_path))
  end
end
