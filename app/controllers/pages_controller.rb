class PagesController < ApplicationController
  before_action :get_categories

  def index
    @cms_page = Comfy::Cms::Page.where(full_path: "/#{params[:cms_page]}").first
    @questions = Question.published.limit(4)
    @events = Event.limit(4)
    @slider = Slider.all
  end

  def show
    @cms_page = Comfy::Cms::Page.where(full_path: "/uslugi#{params[:cms_page]}").first
  end

  def about
    @subpages = Comfy::Cms::Page.where(full_path: "/about").first
    @cms_page = Comfy::Cms::Page.where(full_path: "/about#{params[:cms_page]}").first
  end

  # def specialists
  #   @cms_page = Comfy::Cms::Page.where(full_path: "/specialists#{params[:cms_page]}").first
  # end

  private
  def get_categories
    @cosmetic_categories = Comfy::Cms::Category.where(categorized_type: "CosmeticService")
    @therapeutic_categories = Comfy::Cms::Category.where(categorized_type: "TherapeuticService")
  end
end
