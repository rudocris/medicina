class QuestionsController < ApplicationController
  
  before_action :load_all_tags, only: [:index, :tagged]
  
  def index
    @questions = Question.published
    @question = Question.new
  end

  def tagged
    @questions = Question.tagged_with(params[:tag]).published
    @question = Question.new
    render "index"
  end

  def create
    question = Question.create(question_params)
    redirect_to questions_path
    FeedbacksMailer.send_question(question).deliver
  end
  
  def show
    @question = Question.find(params[:id])
  end

  private
  def question_params
    params.require(:question).permit(:name, :email, :content, :tags)
  end

  def load_all_tags
    @all_tags = Question.tag_counts_on(:tags)
  end
end
