class PriceController < ApplicationController
  def index
    @theapeutic = Price.where(kind: Price::KINDS[0])
    @cosmetic = Price.where(kind: Price::KINDS[1])
  end

  def theapeutic
    @prices = Price.where(kind: Price::KINDS[0])
    render :concreteprice
  end

  def cosmetic
    @prices = Price.where(kind: Price::KINDS[1])
    render :concreteprice
  end

end
