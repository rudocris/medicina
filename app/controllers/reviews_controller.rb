class ReviewsController < ApplicationController
  before_action :load_all_tags
  def index
    @written = Review.written.published
    @videos = Review.videos.published
    @review = Review.new
  end

  def create
    review = Review.create(review_params)
    redirect_to reviews_path
    FeedbacksMailer.send_question(review, subj: "Отзыв").deliver
  end

  def tagged
    reviews = Review.tagged_with(params[:tag])
    
    @written = reviews.written.published
    @videos = reviews.videos.published
    @review = Review.new
    render :index
  end

  def videos
    @videos = Review.videos.published
    @review = Review.new
    render :index
  end

  def written
    @written = Review.written.published
    @review = Review.new
    render :index
  end

  private
  def review_params
    params.require(:review).permit(:name, :email, :content, :tags)
  end

  def load_all_tags
    @all_tags = Review.tag_counts_on(:tags)
  end
end
