class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  before_filter :set_cms_pages
  before_filter :clear_session

  protect_from_forgery with: :exception

  protected 
  def set_cms_pages
    @root_page = Comfy::Cms::Page.where(full_path: '/').includes(children: {children: :children}).first
  end

  def clear_session
    session[:category] = nil
  end
end
