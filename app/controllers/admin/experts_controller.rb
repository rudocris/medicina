class Admin::ExpertsController < Comfy::Admin::Cms::BaseController

  before_action :build_expert,  :only => [:new, :create]
  before_action :load_expert,   :only => [:show, :edit, :update, :destroy]
  before_action :build_meta_tag, only: [:new, :edit]
  def index
    @experts = Expert.order("position").page(params[:page])
  end
  
  def reorder
    params[:expert].each_with_index do |id, position|
      Expert.find(id).update_attribute(:position, position)
    end
    render nothing: true
  end

  def show
    render
  end

  def new
    render
  end

  def edit
    render
  end

  def create
    @expert.save!
    flash[:success] = 'Создан'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка'
    render :action => :new
  end

  def update
    @expert.update_attributes!(expert_params)
    flash[:success] = 'Обновлено'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка'
    render :action => :edit
  end

  def destroy
    @expert.destroy
    flash[:success] = 'Удалено'
    redirect_to :action => :index
  end

protected

  def build_expert
    @expert = Expert.new(expert_params)
  end

  def build_meta_tag
    @expert.build_meta_tag if @expert.meta_tag.nil?
  end

  def load_expert
    @expert = Expert.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'не найдено'
    redirect_to :action => :index
  end

  def expert_params
    params.fetch(:expert, {}).permit(:name, :category, :description, :avatar, :gender, meta_tag_attributes: [:meta_title, :meta_keywords, :meta_description])
  end
end