class Admin::ServicesController < Comfy::Admin::Cms::BaseController
  def index
    @cosmetic_cms_page = Comfy::Cms::Page.find_by_full_path("/services/cosmetic")
    @therapeutic_cms_page = Comfy::Cms::Page.find_by_full_path("/services/theraputhic")
  end
end