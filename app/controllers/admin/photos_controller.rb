class Admin::PhotosController < Comfy::Admin::Cms::BaseController

  before_action :build_photo,  :only => [:new, :create]
  before_action :load_photo,   :only => [:show, :edit, :update, :destroy]

  def index
    @photos = Photo.order("position").page(params[:page])
  end

  def reorder
    params[:photo].each_with_index do |id, position|
      Photo.find(id).update_attribute(:position, position)
    end
    render nothing: true
  end
  
  def show
    render
  end

  def new
    render
  end

  def edit
    render
  end

  def create
    @photo.save!
    flash[:success] = 'Создано'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка'
    render :action => :new
  end

  def update
    @photo.update_attributes!(photo_params)
    flash[:success] = 'Обновлено'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка'
    render :action => :edit
  end

  def destroy
    @photo.destroy
    flash[:success] = 'Удалено'
    redirect_to :action => :index
  end

protected

  def build_photo
    @photo = Photo.new(photo_params)
  end

  def load_photo
    @photo = Photo.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'не найдено'
    redirect_to :action => :index
  end

  def photo_params
    params.fetch(:photo, {}).permit(:title, :image)
  end
end