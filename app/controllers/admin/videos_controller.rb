class Admin::VideosController < Comfy::Admin::Cms::BaseController

  before_action :build_video,  :only => [:new, :create]
  before_action :load_video,   :only => [:show, :edit, :update, :destroy]

  def index
    @videos = Video.order("position").page(params[:page])
  end
  
  def reorder
    params[:video].each_with_index do |id, position|
      Video.find(id).update_attribute(:position, position)
    end
    render nothing: true
  end
  
  def show
    render
  end

  def new
    render
  end

  def edit
    render
  end

  def create
    @video.save!
    flash[:success] = 'Создано '
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка'
    render :action => :new
  end

  def update
    @video.update_attributes!(video_params)
    flash[:success] = 'Обновлено'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка при обновлении'
    render :action => :edit
  end

  def destroy
    @video.destroy
    flash[:success] = 'Удалено'
    redirect_to :action => :index
  end

protected

  def build_video
    @video = Video.new(video_params)
  end

  def load_video
    @video = Video.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'не найдено'
    redirect_to :action => :index
  end

  def video_params
    params.fetch(:video, {}).permit(:title, :url)
  end
end