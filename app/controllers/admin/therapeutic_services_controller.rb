class Admin::TherapeuticServicesController < Comfy::Admin::Cms::BaseController

  before_action :build_therapeutic_service,  :only => [:new, :create]
  before_action :load_therapeutic_service,   :only => [:show, :edit, :update, :destroy]

  def index
    if params[:category]
      @category = Comfy::Cms::Category.find(params[:category])
      @therapeutic_services = TherapeuticService.categorized_with(@category).all
    else
      @therapeutic_services = TherapeuticService.all
    end
  end

  def reorder
    params[:therapeutic_service].each_with_index do |id, position|
      TherapeuticService.find(id).update_attribute(:position, position)
    end
    render nothing: true
  end

  def show
    render
  end

  def new
    render
  end

  def edit
    render
  end

  def create
    @therapeutic_service.save!
    flash[:success] = 'Создано '
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка'
    render :action => :new
  end

  def update
    @therapeutic_service.update_attributes!(therapeutic_service_params)
    flash[:success] = 'Обновлено'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка при обновлении'
    render :action => :edit
  end

  def destroy
    @therapeutic_service.destroy
    flash[:success] = 'Удалено'
    redirect_to :action => :index
  end

protected

  def build_therapeutic_service
    @therapeutic_service = TherapeuticService.new(therapeutic_service_params)
  end

  def load_therapeutic_service
    @therapeutic_service = TherapeuticService.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'не найдено'
    redirect_to :action => :index
  end

  def therapeutic_service_params
    params.fetch(:therapeutic_service, {}).permit!
  end
end