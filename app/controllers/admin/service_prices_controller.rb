class Admin::ServicePricesController < Comfy::Admin::Cms::BaseController

  before_action :build_service_price,  :only => [:new, :create]
  before_action :load_service_price,   :only => [:show, :edit, :update, :destroy]

  def index
    @q = ServicePrice.ransack(params[:q])
    @service_prices = @q.result.page(params[:page])
  end

  def reorder
    params[:service_price].each_with_index do |id, position|
      ServicePrice.find(id).update_attribute(:position, position)
    end
    render nothing: true
  end
  
  def show
    render
  end

  def new
    render
  end

  def edit
    render
  end

  def create
    @service_price.save!
    flash[:success] = 'Создано '
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка'
    render :action => :new
  end

  def update
    @service_price.update_attributes!(service_price_params)
    flash[:success] = 'Обновлено'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка при обновлении'
    render :action => :edit
  end

  def destroy
    @service_price.destroy
    flash[:success] = 'Удалено'
    redirect_to :action => :index
  end

protected

  def build_service_price
    @service_price = ServicePrice.new(service_price_params)
  end

  def load_service_price
    @service_price = ServicePrice.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'не найдено'
    redirect_to :action => :index
  end

  def service_price_params
    params.fetch(:service_price, {}).permit(:title, :price_id, :time, :cost)
  end
end