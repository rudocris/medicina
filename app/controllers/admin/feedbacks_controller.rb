class Admin::FeedbacksController < Comfy::Admin::Cms::BaseController

  before_action :load_feedback, only: [:show, :destroy]

  def index
    @feedbacks = Feedback.page(params[:page])
  end

  def show
    render
  end

  def destroy
    @feedback.destroy
    flash[:success] = 'Сообщение удалено'
    redirect_to :action => :index
  end

protected

  def load_feedback
    @feedback = Feedback.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'Сообщение не найдено'
    redirect_to :action => :index
  end

end