class Admin::CosmeticServicesController < Comfy::Admin::Cms::BaseController

  before_action :build_cosmetic_service,  :only => [:new, :create]
  before_action :load_cosmetic_service,   :only => [:show, :edit, :update, :destroy]

  def index
    
    if params[:category]
      @category = Comfy::Cms::Category.find(params[:category])
      @cosmetic_services = CosmeticService.categorized_with(@category).all
    else
      @cosmetic_services = CosmeticService.all
    end
    
  end

  def reorder
    params[:cosmetic_service].each_with_index do |id, position|
      CosmeticService.find(id).update_attribute(:position, position)
    end
    render nothing: true
  end

  def show
    render
  end

  def new
    render
  end

  def edit
    render
  end

  def create
    @cosmetic_service.save!
    flash[:success] = 'Создано'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка'
    render :action => :new
  end

  def update
    @cosmetic_service.update_attributes!(cosmetic_service_params)
    flash[:success] = 'Обновлено'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка'
    render :action => :edit
  end

  def destroy
    @cosmetic_service.destroy
    flash[:success] = 'Удалено'
    redirect_to :action => :index
  end

protected

  def build_cosmetic_service
    @cosmetic_service = CosmeticService.new(cosmetic_service_params)
  end

  def load_cosmetic_service
    @cosmetic_service = CosmeticService.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'не найдено'
    redirect_to :action => :index
  end

  def cosmetic_service_params
    params.fetch(:cosmetic_service, {}).permit!
  end
end