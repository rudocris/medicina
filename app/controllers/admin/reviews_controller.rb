class Admin::ReviewsController < Comfy::Admin::Cms::BaseController

  before_action :build_review,  :only => [:new, :create]
  before_action :load_review,   :only => [:show, :edit, :update, :destroy]
  before_action :load_tags
  def index
    @reviews = Review.page(params[:page])
  end

  def show
    render
  end

  def new
    render
  end

  def edit
    render
  end

  def create
    @review.save!
    flash[:success] = 'Отзыв добавлен'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка'
    render :action => :new
  end

  def update
    @review.update_attributes!(review_params)
    flash[:success] = 'Обновлено'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка при обновлении'
    render :action => :edit
  end

  def destroy
    @review.destroy
    flash[:success] = 'Удалено'
    redirect_to :action => :index
  end

protected

  def build_review
    @review = Review.new(review_params)
  end

  def load_review
    @review = Review.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'не найдено'
    redirect_to :action => :index
  end

  def load_tags
    @tags = Review.tag_counts_on(:tags)
  end

  def review_params
    params.fetch(:review, {}).permit(:name, :email, :content, :kind, :url, :published, :tag_list)
  end
end