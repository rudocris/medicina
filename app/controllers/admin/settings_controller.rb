class Admin::SettingsController < Comfy::Admin::Cms::BaseController

  before_action :build_settings,  :only => [:new, :create]
  before_action :load_settings,   :only => [:show, :edit, :update, :destroy]

  def index
    @settings = Settings.instance
  end

  def show
    render
  end

  def new
    render
  end

  def edit
    render
  end

  def create
    @settings.save!
    flash[:success] = 'Создано'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Failed to create Settings'
    render :action => :new
  end

  def update
    @settings.update_attributes!(settings_params)
    flash[:success] = 'Настройки обновлены'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Failed to update Settings'
    render :action => :edit
  end

  def destroy
    @settings.destroy
    flash[:success] = 'Settings deleted'
    redirect_to :action => :index
  end

protected

  def build_settings
    @settings = Settings.new(settings_params)
  end

  def load_settings
    @settings = Settings.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'Settings not found'
    redirect_to :action => :index
  end

  def settings_params
    params.fetch(:settings, {}).permit(:singleton_guard, :meta_title, :meta_keywords, :meta_description, :address, :copywrite, :email, :schedule, :phone, :contact_phones, :word_of_wisdom, :notification_emails, :metrika, :get_on_transport, :get_on_foot, :zip_code)
  end
end