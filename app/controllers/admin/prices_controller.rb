class Admin::PricesController < Comfy::Admin::Cms::BaseController

  before_action :build_price,  :only => [:new, :create]
  before_action :load_price,   :only => [:show, :edit, :update, :destroy]

  def index
    @q = Price.ransack(params[:q])
    @prices = @q.result.page(params[:page])
  end
  
  def reorder
    params[:price].each_with_index do |id, position|
      Price.find(id).update_attribute(:position, position)
    end
    render nothing: true
  end

  def show
    render
  end

  def new
    render
  end

  def edit
    render
  end

  def create
    @price.save!
    flash[:success] = 'Создано'
    redirect_to action: :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка'
    render :action => :new
  end

  def update
    @price.update_attributes!(price_params)
    flash[:success] = 'Обновлено'
    redirect_to action: :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка при обновлении'
    render :action => :edit
  end

  def destroy
    @price.destroy
    flash[:success] = 'Удалено'
    redirect_to :action => :index
  end

protected

  def build_price
    @price = Price.new(price_params)
  end

  def load_price
    @price = Price.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'не найдено'
    redirect_to :action => :index
  end

  def price_params
    params.fetch(:price, {}).permit(:title, :kind)
  end
end