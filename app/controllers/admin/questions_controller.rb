class Admin::QuestionsController < Comfy::Admin::Cms::BaseController

  before_action :build_question,  :only => [:new, :create]
  before_action :load_question,   :only => [:show, :edit, :update, :destroy]
  before_action :load_tags

  def index
    @questions = Question.page(params[:page])
  end

  def show
    render
  end

  def new
    render
  end

  def edit
    render
  end

  def create
    @question.save!
    flash[:success] = 'Вопрос создан'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка'
    render :action => :new
  end

  def update
    @question.update_attributes!(question_params)
    flash[:success] = 'Вопрос обновлён'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка при обновлении'
    render :action => :edit
  end

  def destroy
    @question.destroy
    flash[:success] = 'Удалено'
    redirect_to :action => :index
  end

protected

  def build_question
    @question = Question.new(question_params)
  end

  def load_question
    @question = Question.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'не найдено'
    redirect_to :action => :index
  end

  def load_tags
    @tags = Question.tag_counts_on(:tags)
  end

  def question_params
    params.fetch(:question, {}).permit(:name, :email, :content, :answer, :published, :tag_list)
  end
end