class Admin::EventsController < Comfy::Admin::Cms::BaseController

  before_action :build_event,  :only => [:new, :create]
  before_action :load_event,   :only => [:show, :edit, :update, :destroy]

  def index
    @events = Event.page(params[:page])
  end

  def show
    render
  end

  def new
    render
  end

  def edit
    render
  end

  def create
    @event.save!
    flash[:success] = 'Событие добавлено'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка'
    render :action => :new
  end

  def update
    @event.update_attributes!(event_params)
    flash[:success] = 'Обновлено'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка'
    render :action => :edit
  end

  def destroy
    @event.destroy
    flash[:success] = 'Удалено'
    redirect_to :action => :index
  end

protected

  def build_event
    @event = Event.new(event_params)
  end

  def load_event
    @event = Event.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'не найдено'
    redirect_to :action => :index
  end

  def event_params
    params.fetch(:event, {}).permit(:date, :title, :content, :marker, :image)
  end
end