class Admin::SlidersController < Comfy::Admin::Cms::BaseController

  before_action :build_slider,  :only => [:new, :create]
  before_action :load_slider,   :only => [:show, :edit, :update, :destroy]

  def index
    @sliders = Slider.order('position').page(params[:page])
  end
  
  def reorder
    params[:slider].each_with_index do |id, position|
      Slider.find(id).update_attribute(:position, position)
    end
    render nothing: true
  end
  
  def show
    render
  end

  def new
    render
  end

  def edit
    render
  end

  def create
    @slider.save!
    flash[:success] = 'Слайд добавлен'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка'
    render :action => :new
  end

  def update
    @slider.update_attributes!(slider_params)
    flash[:success] = 'Слайд обновлён'
    redirect_to :action => :index
  rescue ActiveRecord::RecordInvalid
    flash.now[:danger] = 'Ошибка'
    render :action => :edit
  end

  def destroy
    @slider.destroy
    flash[:success] = 'Слайд удалён'
    redirect_to :action => :index
  end

protected

  def build_slider
    @slider = Slider.new(slider_params)
  end

  def load_slider
    @slider = Slider.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    flash[:danger] = 'Слайд не найден'
    redirect_to :action => :index
  end

  def slider_params
    params.fetch(:slider, {}).permit(:title, :content, :url, :image)
  end
end