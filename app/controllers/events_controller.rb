class EventsController < ApplicationController
  def index
    @events = Event.all.page(params[:page])
  end

  def show
    @event = Event.find(params[:id])
  end

  def marked
    @events = Event.where(marker: params[:marker]).page(params[:page])
    render "index"
  end
end
