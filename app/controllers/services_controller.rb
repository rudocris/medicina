class ServicesController < ApplicationController
  before_action :get_categories

  def index
    @page = Comfy::Cms::Page.find_by_full_path("/services")
  end

  def type
    @page = Comfy::Cms::Page.find_by_full_path("/services/#{params[:type]}")
    render 'index'
  end

  def category
    @category = Comfy::Cms::Category.find(params[:category_id])
    @services = get_service(@category.categorized_type).categorized_with(@category)
    session[:category] = params[:category_id]
  end

  def show 
    @service = get_service(params[:type]).find(params[:id])
    session[:category] = params[:category]
  end

  private

  def get_service(name)
    if ["CosmeticService", "TherapeuticService"].include? name
      Object.const_get(name)
    else
      CosmeticService
    end
  end

  def get_categories
    @cosmetic_categories = Comfy::Cms::Category.where(categorized_type: "CosmeticService")
    @therapeutic_categories = Comfy::Cms::Category.where(categorized_type: "TherapeuticService")
  end
end
