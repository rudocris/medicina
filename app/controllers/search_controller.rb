class SearchController < ApplicationController
  def index
    @results =  ThinkingSphinx.search params[:q], classes: [Event, Question, Comfy::Cms::Page, CosmeticService, TherapeuticService, Review, Expert], 
                                                                     index: "comfy_cms_page_core,event_core,question_core,cosmetic_service_core,therapeutic_service_core, review_core, expert_core"
  end
end
