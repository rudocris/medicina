class FeedbacksController < ApplicationController
  def create
    feedback = Feedback.create(feedback_params)
    FeedbacksMailer.send_feedback(feedback).deliver
    respond_to do |format|
      format.js
    end
  end

  private

    def feedback_params
      params.require(:feedback).permit(:email, :name, :message)
    end
end
