module Categorized
  extend ActiveSupport::Concern
  
  module ClassMethods
    def categorized_with(category)
      where(id: category.categorizations.map(&:categorized_id))
    end
  end
end