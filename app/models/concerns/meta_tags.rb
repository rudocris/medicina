module MetaTags
  extend ActiveSupport::Concern

  included do
    has_one :meta_tag, as: :meta_taggable
    accepts_nested_attributes_for :meta_tag
  end
  

end