class Photo < ActiveRecord::Base

  has_attached_file :image, :styles => {:medium => "300x260#", :thumb => "100x104>" }, :default_url => "/assets/:style/missing_doctor.jpg"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  validates_presence_of :title, :image
  # -- Relationships --------------------------------------------------------


  # -- Callbacks ------------------------------------------------------------


  # -- Validations ----------------------------------------------------------


  # -- Scopes ---------------------------------------------------------------
  default_scope -> {order('position')}


  # -- Class Methods --------------------------------------------------------


  # -- Instance Methods -----------------------------------------------------


end