class Video < ActiveRecord::Base

  # -- Relationships --------------------------------------------------------
  def youtube_video(width: 640, height: 360)
    id = url.match(/youtube.com.*(?:\/|v=)([^&$]+)/)[1]
    "<iframe width=#{width} height=#{height} src='//www.youtube.com/embed/#{id}' frameborder='0' allowfullscreen></iframe>"
  end
  validates_presence_of :title, :url
  # -- Callbacks ------------------------------------------------------------


  # -- Validations ----------------------------------------------------------


  # -- Scopes ---------------------------------------------------------------
  default_scope -> {order('position')}


  # -- Class Methods --------------------------------------------------------


  # -- Instance Methods -----------------------------------------------------


end