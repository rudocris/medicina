class Settings < ActiveRecord::Base

   validates :singleton_guard, inclusion: { in: [0] }
   validates :singleton_guard, uniqueness: true


  # -- Relationships --------------------------------------------------------


  # -- Callbacks ------------------------------------------------------------


  # -- Validations ----------------------------------------------------------


  # -- Scopes ---------------------------------------------------------------


  # -- Class Methods --------------------------------------------------------
  def self.instance
    find(1)
  rescue ActiveRecord::RecordNotFound
    row = Settings.new
    row.singleton_guard = 0
    row.save!
    row
  end

  # -- Instance Methods -----------------------------------------------------


end