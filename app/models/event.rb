class Event < ActiveRecord::Base
  MARKERS = ["news", "article", "promo"].freeze

  has_attached_file :image, :styles => { :medium => "300x300#", :thumb => "80x80>" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/

  validates_presence_of :date, :title, :content

  # -- Relationships --------------------------------------------------------


  # -- Callbacks ------------------------------------------------------------


  # -- Validations ----------------------------------------------------------


  # -- Scopes ---------------------------------------------------------------


  # -- Class Methods --------------------------------------------------------


  # -- Instance Methods -----------------------------------------------------
  def full_path
    "/events/#{id}"
  end
  
  def label
    title
  end

  def content_cache
    content
  end

end