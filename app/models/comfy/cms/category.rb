class Comfy::Cms::Category < ActiveRecord::Base
  self.table_name = 'comfy_cms_categories'
  
  has_attached_file :picktogram, :styles => { :medium => "64x64>", :thumb => "32x32>" }, :default_url => "/picktograms/missing.png"
  validates_attachment_content_type :picktogram, :content_type => /\Aimage\/.*\Z/

  # -- Relationships --------------------------------------------------------
  belongs_to :site
  has_many :categorizations,
    :dependent => :destroy
    
  # -- Validations ----------------------------------------------------------
  validates :site_id, 
    :presence   => true
  validates :label,
    :presence   => true,
    :uniqueness => { :scope => [:categorized_type, :site_id] }
  validates :categorized_type,
    :presence   => true
    
  # -- Scopes ---------------------------------------------------------------
  default_scope{ order(:position) }
  
  scope :of_type, lambda { |type|
    where(:categorized_type => type)
  }
end