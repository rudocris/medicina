class CosmeticService < ActiveRecord::Base
  include MetaTags
  include Categorized
  
  cms_is_categorized
  
  validates_presence_of :name, :content
  default_scope -> {order('position')}
  
  def full_path
    "/services/#{id}/#{self.class.name}"
  end

  def label
    name
  end

  def content_cache
    content
  end
end