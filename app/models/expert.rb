class Expert < ActiveRecord::Base
  include MetaTags
  GENDERS = ["m", "f"].freeze

  has_attached_file :avatar, :styles => {:lg => "380x505#", :medium => "220x300#", :thumb => "100x100#" }, :default_url => "/assets/:style/missing_doctor.jpg"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  validates_presence_of :name

  # -- Relationships --------------------------------------------------------


  # -- Callbacks ------------------------------------------------------------


  # -- Validations ----------------------------------------------------------


  # -- Scopes ---------------------------------------------------------------
  default_scope -> {order('position')}


  # -- Class Methods --------------------------------------------------------


  # -- Instance Methods -----------------------------------------------------
  def full_path
    "/experts/#{id}"
  end

  def label
    name
  end

  def content_cache
    description
  end
end