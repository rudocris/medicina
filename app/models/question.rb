class Question < ActiveRecord::Base
  acts_as_taggable

  validates_presence_of :name, :email, :content

  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  scope :answered, -> {where.not(answer: [nil, ""])}
  scope :published, -> {where(published: true)}

  def full_path
    "/questions"
  end

  def label
    "Вопросы"
  end

  def content_cache
    content
  end
end