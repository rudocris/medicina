class Price < ActiveRecord::Base
  
  KINDS = ["TherapeuticServices", "CosmeticServices"].freeze
  
  # -- Relationships --------------------------------------------------------
  has_many :service_prices
  # -- Callbacks ------------------------------------------------------------


  # -- Validations ----------------------------------------------------------


  # -- Scopes ---------------------------------------------------------------
  default_scope -> {order('position')}


  # -- Class Methods --------------------------------------------------------


  # -- Instance Methods -----------------------------------------------------


end