class Review < ActiveRecord::Base
  acts_as_taggable

  KINDS = ["video", "written"].freeze

  validates_presence_of :name, :email
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  scope :published, -> {where(published: true)}
  scope :videos, -> {where(kind: "video")}
  scope :written, -> {where(kind: "written")}

  def youtube_video(width: 640, height: 360)
    id = url.match(/youtube.com.*(?:\/|v=)([^&$]+)/)[1]
    "<iframe width=#{width} height=#{height} src='//www.youtube.com/embed/#{id}' frameborder='0' allowfullscreen></iframe>"
  end

  def get_content
    kind == "video" ? youtube_video : content
  end

  def full_path
    "/reviews"
  end

  def label
    name
  end

  def content_cache
    get_content
  end
end