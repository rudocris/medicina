// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require ./vendor/jquery.jcarousel-core.min
//= require ./vendor/jquery.jcarousel-control.min
//= require ./vendor/masonry.min
//= require_tree ./site

$(document).on("page:change", function(){
  $(".fancybox").fancybox();
  
  $('.b-services-gallery .b-services-gallery__list').jcarousel({
    scroll: 1
  });

  $('.b-services-gallery__prev').jcarouselControl({
    target: '-=1'
  });

  $('.b-services-gallery__next').jcarouselControl({
    target: '+=1'
  });

  var language =  {
    requiredFields : 'Поле не должно быть пустым',
    badEmail : 'Некорректно введен Email'
  }
  $.validate({language: language});
  
  $(".b-box__toggle, .b-box__title a").on('click', function(e) {
    var box;
    e.preventDefault();
    box = $(this).parents('.b-box, .b-box-taggable');
    if (box.hasClass('is-active')) {
      return box.removeClass('is-active');
    } else {
      return box.addClass('is-active');
    }
  });



})