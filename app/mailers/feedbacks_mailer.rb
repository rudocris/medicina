class FeedbacksMailer < ActionMailer::Base
  
  default from: "info@klinikamedicina.ru"

  def send_feedback(feedback, to: Settings.email, subj: "Сообщение с сайта klinikamedicina.ru", bcc: Settings.notification_emails)
    @feedback = feedback
    mail(to: to, from: feedback.email, subject: subj, bcc: bcc)
  end

  def send_question(question, subj: "Вопрос с сайта klinikamedicina.ru", to: Settings.instance.email, bcc: Settings.instance.notification_emails)
    @question = question
    mail(to: to, from: question.email, subject: subj, bcc: bcc)
  end

end
 