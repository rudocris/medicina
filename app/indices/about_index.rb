ThinkingSphinx::Index.define :expert, :with => :active_record do
  # fields
  indexes name
  indexes category
  indexes description
end

ThinkingSphinx::Index.define "comfy/cms/page", :with => :active_record do
  # fields
  indexes label
  indexes content_cache
end

ThinkingSphinx::Index.define :cosmetic_service, :with => :active_record do
  # fields
  indexes name
  indexes content
end

ThinkingSphinx::Index.define :therapeutic_service, :with => :active_record do
  # fields
  indexes name
  indexes content
end

ThinkingSphinx::Index.define :question, :with => :active_record do
  # fields
  indexes name
  indexes content
end

ThinkingSphinx::Index.define :event, :with => :active_record do
  # fields
  indexes title
  indexes content
end

ThinkingSphinx::Index.define :review, :with => :active_record do
  # fields
  indexes name
  indexes content
end
