role :app, %w{95.85.14.190}
role :web, %w{95.85.14.190}
role :db,  %w{95.85.14.190}

server '95.85.14.190', user: 'meierlink', roles: %w{web app}