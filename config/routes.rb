Rails.application.routes.draw do

  namespace :admin do
    resources :settings
  end

  get 'videos/index'

  get 'contact/index'

  get 'reviews/index'

  get 'errors/not_found'

  get "/404", to: "errors#not_found"

  resources :feedbacks, only: :create 
  
  resources :price, except: [:new, :update, :create, :show] do
    collection do
      get "theapeutic", as: :theapeutic 
      get "cosmetic", as: :cosmetic
    end
  end

  namespace :admin do
    resources :therapeutic_services do
      post 'reorder', on: :collection
    end
    resources :cosmetic_services do
      post 'reorder', on: :collection
    end
    resources :events
    resources :experts do
      post 'reorder', on: :collection
    end
    resources :questions
    resources :feedbacks
    resources :service_prices do
      post 'reorder', on: :collection
    end
    resources :sliders do
      post 'reorder', on: :collection
    end
    
    resources :photos do
      post 'reorder', on: :collection
    end
    resources :prices do
      post 'reorder', on: :collection
    end
    resources :videos do
      post 'reorder', on: :collection
    end
    resources :reviews
    get 'services/index'
    
  end

  resources :events do
    get ':marker' => "events#marked", as: :marked, on: :collection
  end

  resources :experts 
  
  resources :questions do
    get ':tag' => "questions#tagged", as: :tagged, on: :collection
  end

  resources :reviews do
    collection do
      get 'videos'
      get 'written'
      get ':tag' => "reviews#tagged", as: :tagged
    end
  end

  root 'pages#index'
  get 'pages/show'

  # get 'service/show/:id'
  resources :services do
    get "category/:category_id/" => "services#category", as: :category, on: :collection
    # get "type/:type" => "services#index"
    get ":type" => "services#type", as: :type, on: :collection
    get ":type" => "services#show", as: :show, on: :member
  end
  
  comfy_route :cms_admin, :path => '/admin'

  get '/search', to: 'search#index', as: :search
  
  get '/about*cms_page' => "pages#about", :constraints => { :format => 'html' }
  get '/about' => "pages#about"

  get '/specialists*cms_page' => "pages#specialists"
  get '/specialists' => "pages#specialists"
end
