Rails.application.config.assets.precompile += %w( 
  colorbox.css tinymce/plugins/codemirror/*.css tinymce/plugins/codemirror/*.js
  tinymce_styles_fix.css
  )
