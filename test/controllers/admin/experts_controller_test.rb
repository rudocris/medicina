require_relative '../../test_helper'

class Admin::ExpertsControllerTest < ActionController::TestCase

  def setup
    # TODO: login as admin user
    @expert = experts(:default)
  end

  def test_get_index
    get :index
    assert_response :success
    assert assigns(:experts)
    assert_template :index
  end

  def test_get_show
    get :show, :id => @expert
    assert_response :success
    assert assigns(:expert)
    assert_template :show
  end

  def test_get_show_failure
    get :show, :id => 'invalid'
    assert_response :redirect
    assert_redirected_to :action => :index
    assert_equal 'Expert not found', flash[:danger]
  end

  def test_get_new
    get :new
    assert_response :success
    assert assigns(:expert)
    assert_template :new
    assert_select 'form[action=/admin/experts]'
  end

  def test_get_edit
    get :edit, :id => @expert
    assert_response :success
    assert assigns(:expert)
    assert_template :edit
    assert_select "form[action=/admin/experts/#{@expert.id}]"
  end

  def test_creation
    assert_difference 'Expert.count' do
      post :create, :expert => {
        :category => 'test category',
        :experience => 'test experience',
        :address => 'test address',
        :education => 'test education',
        :speciality => 'test speciality',
        :awards => 'test awards',
      }
      expert = Expert.last
      assert_response :redirect
      assert_redirected_to :action => :show, :id => expert
      assert_equal 'Expert created', flash[:success]
    end
  end

  def test_creation_failure
    assert_no_difference 'Expert.count' do
      post :create, :expert => { }
      assert_response :success
      assert_template :new
      assert_equal 'Failed to create Expert', flash[:danger]
    end
  end

  def test_update
    put :update, :id => @expert, :expert => {
      :category => 'Updated'
    }
    assert_response :redirect
    assert_redirected_to :action => :show, :id => @expert
    assert_equal 'Expert updated', flash[:success]
    @expert.reload
    assert_equal 'Updated', @expert.category
  end

  def test_update_failure
    put :update, :id => @expert, :expert => {
      :category => ''
    }
    assert_response :success
    assert_template :edit
    assert_equal 'Failed to update Expert', flash[:danger]
    @expert.reload
    refute_equal '', @expert.category
  end

  def test_destroy
    assert_difference 'Expert.count', -1 do
      delete :destroy, :id => @expert
      assert_response :redirect
      assert_redirected_to :action => :index
      assert_equal 'Expert deleted', flash[:success]
    end
  end
end