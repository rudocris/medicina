require_relative '../../test_helper'

class Admin::FeedbacksControllerTest < ActionController::TestCase

  def setup
    # TODO: login as admin user
    @feedback = feedbacks(:default)
  end

  def test_get_index
    get :index
    assert_response :success
    assert assigns(:feedbacks)
    assert_template :index
  end

  def test_get_show
    get :show, :id => @feedback
    assert_response :success
    assert assigns(:feedback)
    assert_template :show
  end

  def test_get_show_failure
    get :show, :id => 'invalid'
    assert_response :redirect
    assert_redirected_to :action => :index
    assert_equal 'Feedback not found', flash[:danger]
  end

  def test_get_new
    get :new
    assert_response :success
    assert assigns(:feedback)
    assert_template :new
    assert_select 'form[action=/admin/feedbacks]'
  end

  def test_get_edit
    get :edit, :id => @feedback
    assert_response :success
    assert assigns(:feedback)
    assert_template :edit
    assert_select "form[action=/admin/feedbacks/#{@feedback.id}]"
  end

  def test_creation
    assert_difference 'Feedback.count' do
      post :create, :feedback => {
        :message => 'test message',
      }
      feedback = Feedback.last
      assert_response :redirect
      assert_redirected_to :action => :show, :id => feedback
      assert_equal 'Feedback created', flash[:success]
    end
  end

  def test_creation_failure
    assert_no_difference 'Feedback.count' do
      post :create, :feedback => { }
      assert_response :success
      assert_template :new
      assert_equal 'Failed to create Feedback', flash[:danger]
    end
  end

  def test_update
    put :update, :id => @feedback, :feedback => {
      :message => 'Updated'
    }
    assert_response :redirect
    assert_redirected_to :action => :show, :id => @feedback
    assert_equal 'Feedback updated', flash[:success]
    @feedback.reload
    assert_equal 'Updated', @feedback.message
  end

  def test_update_failure
    put :update, :id => @feedback, :feedback => {
      :message => ''
    }
    assert_response :success
    assert_template :edit
    assert_equal 'Failed to update Feedback', flash[:danger]
    @feedback.reload
    refute_equal '', @feedback.message
  end

  def test_destroy
    assert_difference 'Feedback.count', -1 do
      delete :destroy, :id => @feedback
      assert_response :redirect
      assert_redirected_to :action => :index
      assert_equal 'Feedback deleted', flash[:success]
    end
  end
end