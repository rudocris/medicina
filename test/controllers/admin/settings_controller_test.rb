require_relative '../../test_helper'

class Admin::SettingsControllerTest < ActionController::TestCase

  def setup
    # TODO: login as admin user
    @settings = settings(:default)
  end

  def test_get_index
    get :index
    assert_response :success
    assert assigns(:settings)
    assert_template :index
  end

  def test_get_show
    get :show, :id => @settings
    assert_response :success
    assert assigns(:settings)
    assert_template :show
  end

  def test_get_show_failure
    get :show, :id => 'invalid'
    assert_response :redirect
    assert_redirected_to :action => :index
    assert_equal 'Settings not found', flash[:danger]
  end

  def test_get_new
    get :new
    assert_response :success
    assert assigns(:settings)
    assert_template :new
    assert_select 'form[action=/admin/settings]'
  end

  def test_get_edit
    get :edit, :id => @settings
    assert_response :success
    assert assigns(:settings)
    assert_template :edit
    assert_select "form[action=/admin/settings/#{@settings.id}]"
  end

  def test_creation
    assert_difference 'Settings.count' do
      post :create, :settings => {
        :singleton_guard => 'test singleton_guard',
        :meta_title => 'test meta_title',
        :meta_keywords => 'test meta_keywords',
        :meta_description => 'test meta_description',
        :address => 'test address',
        :copywrite => 'test copywrite',
        :email => 'test email',
        :schedule => 'test schedule',
        :phone => 'test phone',
      }
      settings = Settings.last
      assert_response :redirect
      assert_redirected_to :action => :show, :id => settings
      assert_equal 'Settings created', flash[:success]
    end
  end

  def test_creation_failure
    assert_no_difference 'Settings.count' do
      post :create, :settings => { }
      assert_response :success
      assert_template :new
      assert_equal 'Failed to create Settings', flash[:danger]
    end
  end

  def test_update
    put :update, :id => @settings, :settings => {
      :singleton_guard => 'Updated'
    }
    assert_response :redirect
    assert_redirected_to :action => :show, :id => @settings
    assert_equal 'Settings updated', flash[:success]
    @settings.reload
    assert_equal 'Updated', @settings.singleton_guard
  end

  def test_update_failure
    put :update, :id => @settings, :settings => {
      :singleton_guard => ''
    }
    assert_response :success
    assert_template :edit
    assert_equal 'Failed to update Settings', flash[:danger]
    @settings.reload
    refute_equal '', @settings.singleton_guard
  end

  def test_destroy
    assert_difference 'Settings.count', -1 do
      delete :destroy, :id => @settings
      assert_response :redirect
      assert_redirected_to :action => :index
      assert_equal 'Settings deleted', flash[:success]
    end
  end
end