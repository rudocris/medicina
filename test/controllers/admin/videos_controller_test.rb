require_relative '../../test_helper'

class Admin::VideosControllerTest < ActionController::TestCase

  def setup
    # TODO: login as admin user
    @video = videos(:default)
  end

  def test_get_index
    get :index
    assert_response :success
    assert assigns(:videos)
    assert_template :index
  end

  def test_get_show
    get :show, :id => @video
    assert_response :success
    assert assigns(:video)
    assert_template :show
  end

  def test_get_show_failure
    get :show, :id => 'invalid'
    assert_response :redirect
    assert_redirected_to :action => :index
    assert_equal 'Video not found', flash[:danger]
  end

  def test_get_new
    get :new
    assert_response :success
    assert assigns(:video)
    assert_template :new
    assert_select 'form[action=/admin/videos]'
  end

  def test_get_edit
    get :edit, :id => @video
    assert_response :success
    assert assigns(:video)
    assert_template :edit
    assert_select "form[action=/admin/videos/#{@video.id}]"
  end

  def test_creation
    assert_difference 'Video.count' do
      post :create, :video => {
        :title => 'test title',
        :url => 'test url',
      }
      video = Video.last
      assert_response :redirect
      assert_redirected_to :action => :show, :id => video
      assert_equal 'Video created', flash[:success]
    end
  end

  def test_creation_failure
    assert_no_difference 'Video.count' do
      post :create, :video => { }
      assert_response :success
      assert_template :new
      assert_equal 'Failed to create Video', flash[:danger]
    end
  end

  def test_update
    put :update, :id => @video, :video => {
      :title => 'Updated'
    }
    assert_response :redirect
    assert_redirected_to :action => :show, :id => @video
    assert_equal 'Video updated', flash[:success]
    @video.reload
    assert_equal 'Updated', @video.title
  end

  def test_update_failure
    put :update, :id => @video, :video => {
      :title => ''
    }
    assert_response :success
    assert_template :edit
    assert_equal 'Failed to update Video', flash[:danger]
    @video.reload
    refute_equal '', @video.title
  end

  def test_destroy
    assert_difference 'Video.count', -1 do
      delete :destroy, :id => @video
      assert_response :redirect
      assert_redirected_to :action => :index
      assert_equal 'Video deleted', flash[:success]
    end
  end
end