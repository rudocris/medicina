require_relative '../../test_helper'

class Admin::ReviewsControllerTest < ActionController::TestCase

  def setup
    # TODO: login as admin user
    @review = reviews(:default)
  end

  def test_get_index
    get :index
    assert_response :success
    assert assigns(:reviews)
    assert_template :index
  end

  def test_get_show
    get :show, :id => @review
    assert_response :success
    assert assigns(:review)
    assert_template :show
  end

  def test_get_show_failure
    get :show, :id => 'invalid'
    assert_response :redirect
    assert_redirected_to :action => :index
    assert_equal 'Review not found', flash[:danger]
  end

  def test_get_new
    get :new
    assert_response :success
    assert assigns(:review)
    assert_template :new
    assert_select 'form[action=/admin/reviews]'
  end

  def test_get_edit
    get :edit, :id => @review
    assert_response :success
    assert assigns(:review)
    assert_template :edit
    assert_select "form[action=/admin/reviews/#{@review.id}]"
  end

  def test_creation
    assert_difference 'Review.count' do
      post :create, :review => {
        :name => 'test name',
        :email => 'test email',
        :content => 'test content',
        :kind => 'test kind',
      }
      review = Review.last
      assert_response :redirect
      assert_redirected_to :action => :show, :id => review
      assert_equal 'Review created', flash[:success]
    end
  end

  def test_creation_failure
    assert_no_difference 'Review.count' do
      post :create, :review => { }
      assert_response :success
      assert_template :new
      assert_equal 'Failed to create Review', flash[:danger]
    end
  end

  def test_update
    put :update, :id => @review, :review => {
      :name => 'Updated'
    }
    assert_response :redirect
    assert_redirected_to :action => :show, :id => @review
    assert_equal 'Review updated', flash[:success]
    @review.reload
    assert_equal 'Updated', @review.name
  end

  def test_update_failure
    put :update, :id => @review, :review => {
      :name => ''
    }
    assert_response :success
    assert_template :edit
    assert_equal 'Failed to update Review', flash[:danger]
    @review.reload
    refute_equal '', @review.name
  end

  def test_destroy
    assert_difference 'Review.count', -1 do
      delete :destroy, :id => @review
      assert_response :redirect
      assert_redirected_to :action => :index
      assert_equal 'Review deleted', flash[:success]
    end
  end
end