require_relative '../../test_helper'

class Admin::CosmeticServicesControllerTest < ActionController::TestCase

  def setup
    # TODO: login as admin user
    @cosmetic_service = cosmetic_services(:default)
  end

  def test_get_index
    get :index
    assert_response :success
    assert assigns(:cosmetic_services)
    assert_template :index
  end

  def test_get_show
    get :show, :id => @cosmetic_service
    assert_response :success
    assert assigns(:cosmetic_service)
    assert_template :show
  end

  def test_get_show_failure
    get :show, :id => 'invalid'
    assert_response :redirect
    assert_redirected_to :action => :index
    assert_equal 'Cosmetic Service not found', flash[:danger]
  end

  def test_get_new
    get :new
    assert_response :success
    assert assigns(:cosmetic_service)
    assert_template :new
    assert_select 'form[action=/admin/cosmetic_services]'
  end

  def test_get_edit
    get :edit, :id => @cosmetic_service
    assert_response :success
    assert assigns(:cosmetic_service)
    assert_template :edit
    assert_select "form[action=/admin/cosmetic_services/#{@cosmetic_service.id}]"
  end

  def test_creation
    assert_difference 'CosmeticService.count' do
      post :create, :cosmetic_service => {
        :name => 'test name',
        :content => 'test content',
      }
      cosmetic_service = CosmeticService.last
      assert_response :redirect
      assert_redirected_to :action => :show, :id => cosmetic_service
      assert_equal 'Cosmetic Service created', flash[:success]
    end
  end

  def test_creation_failure
    assert_no_difference 'CosmeticService.count' do
      post :create, :cosmetic_service => { }
      assert_response :success
      assert_template :new
      assert_equal 'Failed to create Cosmetic Service', flash[:danger]
    end
  end

  def test_update
    put :update, :id => @cosmetic_service, :cosmetic_service => {
      :name => 'Updated'
    }
    assert_response :redirect
    assert_redirected_to :action => :show, :id => @cosmetic_service
    assert_equal 'Cosmetic Service updated', flash[:success]
    @cosmetic_service.reload
    assert_equal 'Updated', @cosmetic_service.name
  end

  def test_update_failure
    put :update, :id => @cosmetic_service, :cosmetic_service => {
      :name => ''
    }
    assert_response :success
    assert_template :edit
    assert_equal 'Failed to update Cosmetic Service', flash[:danger]
    @cosmetic_service.reload
    refute_equal '', @cosmetic_service.name
  end

  def test_destroy
    assert_difference 'CosmeticService.count', -1 do
      delete :destroy, :id => @cosmetic_service
      assert_response :redirect
      assert_redirected_to :action => :index
      assert_equal 'Cosmetic Service deleted', flash[:success]
    end
  end
end