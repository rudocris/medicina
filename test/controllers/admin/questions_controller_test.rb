require_relative '../../test_helper'

class Admin::QuestionsControllerTest < ActionController::TestCase

  def setup
    # TODO: login as admin user
    @question = questions(:default)
  end

  def test_get_index
    get :index
    assert_response :success
    assert assigns(:questions)
    assert_template :index
  end

  def test_get_show
    get :show, :id => @question
    assert_response :success
    assert assigns(:question)
    assert_template :show
  end

  def test_get_show_failure
    get :show, :id => 'invalid'
    assert_response :redirect
    assert_redirected_to :action => :index
    assert_equal 'Question not found', flash[:danger]
  end

  def test_get_new
    get :new
    assert_response :success
    assert assigns(:question)
    assert_template :new
    assert_select 'form[action=/admin/questions]'
  end

  def test_get_edit
    get :edit, :id => @question
    assert_response :success
    assert assigns(:question)
    assert_template :edit
    assert_select "form[action=/admin/questions/#{@question.id}]"
  end

  def test_creation
    assert_difference 'Question.count' do
      post :create, :question => {
        :name => 'test name',
        :email => 'test email',
        :content => 'test content',
      }
      question = Question.last
      assert_response :redirect
      assert_redirected_to :action => :show, :id => question
      assert_equal 'Question created', flash[:success]
    end
  end

  def test_creation_failure
    assert_no_difference 'Question.count' do
      post :create, :question => { }
      assert_response :success
      assert_template :new
      assert_equal 'Failed to create Question', flash[:danger]
    end
  end

  def test_update
    put :update, :id => @question, :question => {
      :name => 'Updated'
    }
    assert_response :redirect
    assert_redirected_to :action => :show, :id => @question
    assert_equal 'Question updated', flash[:success]
    @question.reload
    assert_equal 'Updated', @question.name
  end

  def test_update_failure
    put :update, :id => @question, :question => {
      :name => ''
    }
    assert_response :success
    assert_template :edit
    assert_equal 'Failed to update Question', flash[:danger]
    @question.reload
    refute_equal '', @question.name
  end

  def test_destroy
    assert_difference 'Question.count', -1 do
      delete :destroy, :id => @question
      assert_response :redirect
      assert_redirected_to :action => :index
      assert_equal 'Question deleted', flash[:success]
    end
  end
end