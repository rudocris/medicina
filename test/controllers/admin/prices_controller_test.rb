require_relative '../../test_helper'

class Admin::PricesControllerTest < ActionController::TestCase

  def setup
    # TODO: login as admin user
    @price = prices(:default)
  end

  def test_get_index
    get :index
    assert_response :success
    assert assigns(:prices)
    assert_template :index
  end

  def test_get_show
    get :show, :id => @price
    assert_response :success
    assert assigns(:price)
    assert_template :show
  end

  def test_get_show_failure
    get :show, :id => 'invalid'
    assert_response :redirect
    assert_redirected_to :action => :index
    assert_equal 'Price not found', flash[:danger]
  end

  def test_get_new
    get :new
    assert_response :success
    assert assigns(:price)
    assert_template :new
    assert_select 'form[action=/admin/prices]'
  end

  def test_get_edit
    get :edit, :id => @price
    assert_response :success
    assert assigns(:price)
    assert_template :edit
    assert_select "form[action=/admin/prices/#{@price.id}]"
  end

  def test_creation
    assert_difference 'Price.count' do
      post :create, :price => {
        :title => 'test title',
      }
      price = Price.last
      assert_response :redirect
      assert_redirected_to :action => :show, :id => price
      assert_equal 'Price created', flash[:success]
    end
  end

  def test_creation_failure
    assert_no_difference 'Price.count' do
      post :create, :price => { }
      assert_response :success
      assert_template :new
      assert_equal 'Failed to create Price', flash[:danger]
    end
  end

  def test_update
    put :update, :id => @price, :price => {
      :title => 'Updated'
    }
    assert_response :redirect
    assert_redirected_to :action => :show, :id => @price
    assert_equal 'Price updated', flash[:success]
    @price.reload
    assert_equal 'Updated', @price.title
  end

  def test_update_failure
    put :update, :id => @price, :price => {
      :title => ''
    }
    assert_response :success
    assert_template :edit
    assert_equal 'Failed to update Price', flash[:danger]
    @price.reload
    refute_equal '', @price.title
  end

  def test_destroy
    assert_difference 'Price.count', -1 do
      delete :destroy, :id => @price
      assert_response :redirect
      assert_redirected_to :action => :index
      assert_equal 'Price deleted', flash[:success]
    end
  end
end