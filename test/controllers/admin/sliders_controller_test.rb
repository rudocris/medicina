require_relative '../../test_helper'

class Admin::SlidersControllerTest < ActionController::TestCase

  def setup
    # TODO: login as admin user
    @slider = sliders(:default)
  end

  def test_get_index
    get :index
    assert_response :success
    assert assigns(:sliders)
    assert_template :index
  end

  def test_get_show
    get :show, :id => @slider
    assert_response :success
    assert assigns(:slider)
    assert_template :show
  end

  def test_get_show_failure
    get :show, :id => 'invalid'
    assert_response :redirect
    assert_redirected_to :action => :index
    assert_equal 'Slider not found', flash[:danger]
  end

  def test_get_new
    get :new
    assert_response :success
    assert assigns(:slider)
    assert_template :new
    assert_select 'form[action=/admin/sliders]'
  end

  def test_get_edit
    get :edit, :id => @slider
    assert_response :success
    assert assigns(:slider)
    assert_template :edit
    assert_select "form[action=/admin/sliders/#{@slider.id}]"
  end

  def test_creation
    assert_difference 'Slider.count' do
      post :create, :slider => {
        :title => 'test title',
        :content => 'test content',
        :url => 'test url',
      }
      slider = Slider.last
      assert_response :redirect
      assert_redirected_to :action => :show, :id => slider
      assert_equal 'Slider created', flash[:success]
    end
  end

  def test_creation_failure
    assert_no_difference 'Slider.count' do
      post :create, :slider => { }
      assert_response :success
      assert_template :new
      assert_equal 'Failed to create Slider', flash[:danger]
    end
  end

  def test_update
    put :update, :id => @slider, :slider => {
      :title => 'Updated'
    }
    assert_response :redirect
    assert_redirected_to :action => :show, :id => @slider
    assert_equal 'Slider updated', flash[:success]
    @slider.reload
    assert_equal 'Updated', @slider.title
  end

  def test_update_failure
    put :update, :id => @slider, :slider => {
      :title => ''
    }
    assert_response :success
    assert_template :edit
    assert_equal 'Failed to update Slider', flash[:danger]
    @slider.reload
    refute_equal '', @slider.title
  end

  def test_destroy
    assert_difference 'Slider.count', -1 do
      delete :destroy, :id => @slider
      assert_response :redirect
      assert_redirected_to :action => :index
      assert_equal 'Slider deleted', flash[:success]
    end
  end
end