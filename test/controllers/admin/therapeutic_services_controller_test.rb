require_relative '../../test_helper'

class Admin::TherapeuticServicesControllerTest < ActionController::TestCase

  def setup
    # TODO: login as admin user
    @therapeutic_service = therapeutic_services(:default)
  end

  def test_get_index
    get :index
    assert_response :success
    assert assigns(:therapeutic_services)
    assert_template :index
  end

  def test_get_show
    get :show, :id => @therapeutic_service
    assert_response :success
    assert assigns(:therapeutic_service)
    assert_template :show
  end

  def test_get_show_failure
    get :show, :id => 'invalid'
    assert_response :redirect
    assert_redirected_to :action => :index
    assert_equal 'Therapeutic Service not found', flash[:danger]
  end

  def test_get_new
    get :new
    assert_response :success
    assert assigns(:therapeutic_service)
    assert_template :new
    assert_select 'form[action=/admin/therapeutic_services]'
  end

  def test_get_edit
    get :edit, :id => @therapeutic_service
    assert_response :success
    assert assigns(:therapeutic_service)
    assert_template :edit
    assert_select "form[action=/admin/therapeutic_services/#{@therapeutic_service.id}]"
  end

  def test_creation
    assert_difference 'TherapeuticService.count' do
      post :create, :therapeutic_service => {
        :name => 'test name',
        :content => 'test content',
      }
      therapeutic_service = TherapeuticService.last
      assert_response :redirect
      assert_redirected_to :action => :show, :id => therapeutic_service
      assert_equal 'Therapeutic Service created', flash[:success]
    end
  end

  def test_creation_failure
    assert_no_difference 'TherapeuticService.count' do
      post :create, :therapeutic_service => { }
      assert_response :success
      assert_template :new
      assert_equal 'Failed to create Therapeutic Service', flash[:danger]
    end
  end

  def test_update
    put :update, :id => @therapeutic_service, :therapeutic_service => {
      :name => 'Updated'
    }
    assert_response :redirect
    assert_redirected_to :action => :show, :id => @therapeutic_service
    assert_equal 'Therapeutic Service updated', flash[:success]
    @therapeutic_service.reload
    assert_equal 'Updated', @therapeutic_service.name
  end

  def test_update_failure
    put :update, :id => @therapeutic_service, :therapeutic_service => {
      :name => ''
    }
    assert_response :success
    assert_template :edit
    assert_equal 'Failed to update Therapeutic Service', flash[:danger]
    @therapeutic_service.reload
    refute_equal '', @therapeutic_service.name
  end

  def test_destroy
    assert_difference 'TherapeuticService.count', -1 do
      delete :destroy, :id => @therapeutic_service
      assert_response :redirect
      assert_redirected_to :action => :index
      assert_equal 'Therapeutic Service deleted', flash[:success]
    end
  end
end