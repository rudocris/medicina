require_relative '../../test_helper'

class Admin::ServicePricesControllerTest < ActionController::TestCase

  def setup
    # TODO: login as admin user
    @service_price = service_prices(:default)
  end

  def test_get_index
    get :index
    assert_response :success
    assert assigns(:service_prices)
    assert_template :index
  end

  def test_get_show
    get :show, :id => @service_price
    assert_response :success
    assert assigns(:service_price)
    assert_template :show
  end

  def test_get_show_failure
    get :show, :id => 'invalid'
    assert_response :redirect
    assert_redirected_to :action => :index
    assert_equal 'Service Price not found', flash[:danger]
  end

  def test_get_new
    get :new
    assert_response :success
    assert assigns(:service_price)
    assert_template :new
    assert_select 'form[action=/admin/service_prices]'
  end

  def test_get_edit
    get :edit, :id => @service_price
    assert_response :success
    assert assigns(:service_price)
    assert_template :edit
    assert_select "form[action=/admin/service_prices/#{@service_price.id}]"
  end

  def test_creation
    assert_difference 'ServicePrice.count' do
      post :create, :service_price => {
        :title => 'test title',
        :price_id => 'test price_id',
        :time => 'test time',
        :cost => 'test cost',
      }
      service_price = ServicePrice.last
      assert_response :redirect
      assert_redirected_to :action => :show, :id => service_price
      assert_equal 'Service Price created', flash[:success]
    end
  end

  def test_creation_failure
    assert_no_difference 'ServicePrice.count' do
      post :create, :service_price => { }
      assert_response :success
      assert_template :new
      assert_equal 'Failed to create Service Price', flash[:danger]
    end
  end

  def test_update
    put :update, :id => @service_price, :service_price => {
      :title => 'Updated'
    }
    assert_response :redirect
    assert_redirected_to :action => :show, :id => @service_price
    assert_equal 'Service Price updated', flash[:success]
    @service_price.reload
    assert_equal 'Updated', @service_price.title
  end

  def test_update_failure
    put :update, :id => @service_price, :service_price => {
      :title => ''
    }
    assert_response :success
    assert_template :edit
    assert_equal 'Failed to update Service Price', flash[:danger]
    @service_price.reload
    refute_equal '', @service_price.title
  end

  def test_destroy
    assert_difference 'ServicePrice.count', -1 do
      delete :destroy, :id => @service_price
      assert_response :redirect
      assert_redirected_to :action => :index
      assert_equal 'Service Price deleted', flash[:success]
    end
  end
end