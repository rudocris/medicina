require_relative '../test_helper'

class EventTest < ActiveSupport::TestCase

  def test_fixtures_validity
    Event.all.each do |event|
      assert event.valid?, event.errors.inspect
    end
  end

  def test_validation
    event = Event.new
    assert event.invalid?
    assert_errors_on event, :date, :title, :content, :type
  end

  def test_creation
    assert_difference 'Event.count' do
      Event.create(
        :date => 'test date',
        :title => 'test title',
        :content => 'test content',
        :type => 'test type',
      )
    end
  end

end