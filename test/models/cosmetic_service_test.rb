require_relative '../test_helper'

class CosmeticServiceTest < ActiveSupport::TestCase

  def test_fixtures_validity
    CosmeticService.all.each do |cosmetic_service|
      assert cosmetic_service.valid?, cosmetic_service.errors.inspect
    end
  end

  def test_validation
    cosmetic_service = CosmeticService.new
    assert cosmetic_service.invalid?
    assert_errors_on cosmetic_service, :name, :content
  end

  def test_creation
    assert_difference 'CosmeticService.count' do
      CosmeticService.create(
        :name => 'test name',
        :content => 'test content',
      )
    end
  end

end