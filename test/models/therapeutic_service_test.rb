require_relative '../test_helper'

class TherapeuticServiceTest < ActiveSupport::TestCase

  def test_fixtures_validity
    TherapeuticService.all.each do |therapeutic_service|
      assert therapeutic_service.valid?, therapeutic_service.errors.inspect
    end
  end

  def test_validation
    therapeutic_service = TherapeuticService.new
    assert therapeutic_service.invalid?
    assert_errors_on therapeutic_service, :name, :content
  end

  def test_creation
    assert_difference 'TherapeuticService.count' do
      TherapeuticService.create(
        :name => 'test name',
        :content => 'test content',
      )
    end
  end

end