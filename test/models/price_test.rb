require_relative '../test_helper'

class PriceTest < ActiveSupport::TestCase

  def test_fixtures_validity
    Price.all.each do |price|
      assert price.valid?, price.errors.inspect
    end
  end

  def test_validation
    price = Price.new
    assert price.invalid?
    assert_errors_on price, :title
  end

  def test_creation
    assert_difference 'Price.count' do
      Price.create(
        :title => 'test title',
      )
    end
  end

end