require_relative '../test_helper'

class QuestionTest < ActiveSupport::TestCase

  def test_fixtures_validity
    Question.all.each do |question|
      assert question.valid?, question.errors.inspect
    end
  end

  def test_validation
    question = Question.new
    assert question.invalid?
    assert_errors_on question, :name, :email, :content
  end

  def test_creation
    assert_difference 'Question.count' do
      Question.create(
        :name => 'test name',
        :email => 'test email',
        :content => 'test content',
      )
    end
  end

end