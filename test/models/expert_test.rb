require_relative '../test_helper'

class ExpertTest < ActiveSupport::TestCase

  def test_fixtures_validity
    Expert.all.each do |expert|
      assert expert.valid?, expert.errors.inspect
    end
  end

  def test_validation
    expert = Expert.new
    assert expert.invalid?
    assert_errors_on expert, :category, :experience, :address, :education, :speciality, :awards
  end

  def test_creation
    assert_difference 'Expert.count' do
      Expert.create(
        :category => 'test category',
        :experience => 'test experience',
        :address => 'test address',
        :education => 'test education',
        :speciality => 'test speciality',
        :awards => 'test awards',
      )
    end
  end

end