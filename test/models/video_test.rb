require_relative '../test_helper'

class VideoTest < ActiveSupport::TestCase

  def test_fixtures_validity
    Video.all.each do |video|
      assert video.valid?, video.errors.inspect
    end
  end

  def test_validation
    video = Video.new
    assert video.invalid?
    assert_errors_on video, :title, :url
  end

  def test_creation
    assert_difference 'Video.count' do
      Video.create(
        :title => 'test title',
        :url => 'test url',
      )
    end
  end

end