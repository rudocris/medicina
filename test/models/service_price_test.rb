require_relative '../test_helper'

class ServicePriceTest < ActiveSupport::TestCase

  def test_fixtures_validity
    ServicePrice.all.each do |service_price|
      assert service_price.valid?, service_price.errors.inspect
    end
  end

  def test_validation
    service_price = ServicePrice.new
    assert service_price.invalid?
    assert_errors_on service_price, :title, :price_id, :time, :cost
  end

  def test_creation
    assert_difference 'ServicePrice.count' do
      ServicePrice.create(
        :title => 'test title',
        :price_id => 'test price_id',
        :time => 'test time',
        :cost => 'test cost',
      )
    end
  end

end