require_relative '../test_helper'

class FeedbackTest < ActiveSupport::TestCase

  def test_fixtures_validity
    Feedback.all.each do |feedback|
      assert feedback.valid?, feedback.errors.inspect
    end
  end

  def test_validation
    feedback = Feedback.new
    assert feedback.invalid?
    assert_errors_on feedback, :message
  end

  def test_creation
    assert_difference 'Feedback.count' do
      Feedback.create(
        :message => 'test message',
      )
    end
  end

end