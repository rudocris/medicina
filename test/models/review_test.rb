require_relative '../test_helper'

class ReviewTest < ActiveSupport::TestCase

  def test_fixtures_validity
    Review.all.each do |review|
      assert review.valid?, review.errors.inspect
    end
  end

  def test_validation
    review = Review.new
    assert review.invalid?
    assert_errors_on review, :name, :email, :content, :kind
  end

  def test_creation
    assert_difference 'Review.count' do
      Review.create(
        :name => 'test name',
        :email => 'test email',
        :content => 'test content',
        :kind => 'test kind',
      )
    end
  end

end