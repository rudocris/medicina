require_relative '../test_helper'

class SliderTest < ActiveSupport::TestCase

  def test_fixtures_validity
    Slider.all.each do |slider|
      assert slider.valid?, slider.errors.inspect
    end
  end

  def test_validation
    slider = Slider.new
    assert slider.invalid?
    assert_errors_on slider, :title, :content, :url
  end

  def test_creation
    assert_difference 'Slider.count' do
      Slider.create(
        :title => 'test title',
        :content => 'test content',
        :url => 'test url',
      )
    end
  end

end