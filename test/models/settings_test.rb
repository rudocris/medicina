require_relative '../test_helper'

class SettingsTest < ActiveSupport::TestCase

  def test_fixtures_validity
    Settings.all.each do |settings|
      assert settings.valid?, settings.errors.inspect
    end
  end

  def test_validation
    settings = Settings.new
    assert settings.invalid?
    assert_errors_on settings, :singleton_guard, :meta_title, :meta_keywords, :meta_description, :address, :copywrite, :email, :schedule, :phone
  end

  def test_creation
    assert_difference 'Settings.count' do
      Settings.create(
        :singleton_guard => 'test singleton_guard',
        :meta_title => 'test meta_title',
        :meta_keywords => 'test meta_keywords',
        :meta_description => 'test meta_description',
        :address => 'test address',
        :copywrite => 'test copywrite',
        :email => 'test email',
        :schedule => 'test schedule',
        :phone => 'test phone',
      )
    end
  end

end